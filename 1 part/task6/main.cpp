#include <iostream>
using namespace std;

int median(const int& first,const int& second,const int& third,int *_array)
{
    return  (_array[first] > _array[second])? ((_array[third]<_array[second])?second:((_array[third]<_array[first])?third:first)): ( (_array[first]>_array[third])?first:(_array[second]>_array[third])?third:second);
};

int Partition(int *_array,const int& size,bool (*cmp)(const int&,const int&))
{
    if(size<=1)
        return 0;
    int pivot = median(0,size/2,size-1,_array);
    int i=0 , j=0;
    swap(_array[pivot],_array[size-1]);
    while((j<size))
    {
        if(cmp(_array[j],_array[size-1])) //<
        {
            swap(_array[i],_array[j]);
            ++i;
        }
        ++j;
    }
    return (i-1);
};


bool Compare(const int& first, const int& second)
{
    return first <= second;
};

void QuickSort(int *_array,const int& size,bool (*cmp)(const int&,const int&), const int& stoppos)
{
    int _size=size,_stoppos=stoppos, _bias=0;
    int pivot=Partition(_array,_size,cmp);

    while(pivot!=_stoppos)
    {
        if(pivot<_stoppos)
        {
            _size-=(pivot+1);
            _stoppos-=(pivot+1);
            _bias+=(pivot+1);
            pivot=Partition(_array+_bias,_size,cmp);
        }

        else
        {
            _size=pivot;
            pivot=Partition(_array+_bias,_size,cmp);
        }

    }

    cout <<_array[stoppos];
}

int main() {
    int command =0, stoppos=0;
    cin >> command >> stoppos;
    int *array=new int[command];
    for(int i = 0; i < command ; ++i)
        cin >> array[i];
    QuickSort(array,command,Compare,stoppos);
    return 0;
}