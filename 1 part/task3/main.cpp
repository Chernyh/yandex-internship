#include <iostream>
using namespace std;
#define QueueuDefaultSize 1000000

/*class QueueModified{
public:
     QueueModified(): headfirst(0) ,headsecond(0) , sizefirst(QueueuDefaultSize) ,sizesecond(QueueuDefaultSize)
    {
        first = new int[sizefirst];
        second = new int[sizesecond];
    };

    ~QueueModified()
    {
        delete[]first;
        delete[]second;
        headfirst=headsecond=0;
        sizesecond=sizefirst=0;
    };

    void PushBack(const int& value)
    {
        if(headfirst==sizefirst)
            MoveFirstToSecond();
        first[headfirst++]=value;
    };

    int PopFront()
    {
        if(IsEmptyBoth())
            return -1;
        if(IsEmptySecond())
            MoveFirstToSecond();
        return second[--headsecond];
    };

private:


    void ResizeSecond(const int& value)
    {
        int *tmp = new int[sizesecond*2+value];
        for(int i = 0 ; i< headsecond; ++i)
            tmp[i+value]=second[i];
        delete[]second;
        second=tmp;
        sizesecond*=2;
        sizesecond+=value;
    };

    bool IsEmptyFirst()
    {
        return headfirst==0;
    };

    bool IsEmptySecond()
    {
        return headsecond==0;
    };

    bool IsEmptyBoth()
    {
       return (IsEmptyFirst() && IsEmptySecond());
    };

    void MoveFirstToSecond()
    {
        int transfer =0 , i=0;
        if(headfirst>(sizesecond-headsecond))
        {
            ResizeSecond(headfirst);
            transfer=headsecond;
            headsecond=0;

        }

        while(i<headsecond)
        {
            second[headsecond-1-i+headfirst] = second[headsecond-1-i];
            ++i;
        }
        i=0;
        while(i<headfirst)
        {
            second[i]=first[headfirst-i-1];
            ++i;
        }
        headsecond+=(transfer+headfirst);
        headfirst=0;


    };

    int *first , *second , headfirst , headsecond , sizefirst , sizesecond;
};*/

class Steck
{
public:
    Steck():head(0),size(QueueuDefaultSize)
    {
        array=new int[size];
    };
    ~Steck()
    {
        delete[]array;
        head=0;
        size=0;
    };
    int ReturnHead()
    {
        return head;
    };
    void PushBack(int const element)
    {
        if(head==size)
            Resize();
        array[head++]=element;
    };
    int PopFront()
    {
        return array[--head];
    };
private:
    void Resize()
    {
        int *buff=new int[size*2];
        for(int i = 0 ; i< size ; ++i)
            buff[i]=array[i];
        delete[]array;
        array=buff;
        size*=2;
    };
   int *array, head, size;
};
class QueueModified{
public:
    QueueModified()
    {
    };
    ~QueueModified()
    {
    };
    void PushBack(const int value)
    {
        first.PushBack(value);
    };
    int PopFront()
    {
        if(second.ReturnHead()==0)
        {
            if(first.ReturnHead()!=0)
            {
                int firstSize=first.ReturnHead()-1;
                while(firstSize>=0)
                {
                    second.PushBack(first.PopFront());
                    --firstSize;
                }
            }
            else
                return -1;

        }
        return second.PopFront();
    };
private:
    Steck first,second;
};


int main() {
    int command = 0 , send , value , answer;
    QueueModified Queue;
    cin >> command;
    answer=command;
    for(int i = 0 ; i < command ; ++i)
    {
        cin >> send >> value;

        if (send==2 && Queue.PopFront() == value)
            --answer;

        if(send==3)
        {
            --answer;
            Queue.PushBack(value);
        }
    }



    if(!answer)
        cout << "YES";
    else
        cout <<"NO";
    return 0;
}