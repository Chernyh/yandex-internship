#include <iostream>
#include <cstring>

using namespace std;

struct Line{
    int left;
    int right;

    Line(): left(0),right(0){};
    Line(const int& _right,const int& _left): left(_left),right(_right){};
    Line(const Line& copy): left(copy.left),right(copy.right){};
};

class Comp {
public:
    bool operator()(const Line &first, const Line &second )
    {
        return first.left < second.left;
    }
};
template<class Comparator=Comp>
class MergeLine{
public:
    MergeLine():size(0),Compare(Comparator()){};
    MergeLine(const Line *_array,const int& _size, bool (*cmp)(const Line&, const Line&)):size(_size),Compare(Comparator())
    {
        array=new Line[size];
        for(int i = 0 ; i< size ; ++i)
            array[i]=_array[i];
    };
    ~MergeLine()
    {
        size=0;
        delete[]array;

    }

    int Count()
    {
      MergeSort(array,size);
      Line comparable(array[0]);
      int length=0,i=1;
      length+=(comparable.right-comparable.left);
      while(i<size)
      {
          if(comparable.right>array[i].left && comparable.right<array[i].right && comparable.left<array[i].left)
          {
              length+=(array[i].right-comparable.right);
              comparable.right=array[i].right;
          }
          else
              if(comparable.right<=array[i].left)
              {
                  length+=(array[i].right-array[i].left);
                  comparable.left=array[i].left;
                  comparable.right=array[i].right;
              }

          ++i;
      }
      return length;
    };
private:

    void Merge(Line *first,const int& sizefirst, Line* second,const int& sizesecond,Line *copied)
    {
        int i=0 , j=0;
        while(!(i==sizefirst && j==sizesecond))
        {
            if(Compare(first[i],second[j]))
            {
                if(i<sizefirst)
                {
                    copied[i+j]=first[i];
                    ++i;
                }
                else
                {
                    while(j<sizesecond)
                    {
                        copied[i+j]=second[j];
                        ++j;
                    }

                }

            }
            else
            {
                if(j<sizesecond)
                {

                    copied[i+j]=second[j];
                    ++j;
                }
                else
                {
                    while(i<sizefirst)
                    {
                        copied[i+j]=first[i];
                        ++i;
                    }
                }

            }
        }
    };
    void MergeSort(Line* _array,const int& _size)
    {
        if(_size <=1)
            return;
        int firstlen=_size/2;
        int secondlen=_size-firstlen;
        MergeSort(_array,firstlen);
        MergeSort(_array+firstlen,secondlen);
        Line *copy= new Line[_size];
        Merge(_array,firstlen,_array+firstlen,secondlen,copy);
        memcpy(_array,copy,sizeof(Line)*_size);
        delete[]copy;
    };
    Comparator Compare;
    Line* array;
    int size;
};

bool cmp(const Line& left,const Line& right)
{
    return left.left < right.left;
};

int main() {
    int command =0;
    Line *ptr;
    cin >> command;
    ptr=new Line[command];
    for (int i = 0; i < command; ++i)
        cin >> ptr[i].left >> ptr[i].right;

    MergeLine<> merged(ptr,command,cmp);
    cout << merged.Count();
    return 0;
}