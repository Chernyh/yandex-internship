#include <iostream>
using namespace std;




int Partition(int64_t *array,const int64_t n,int64_t &mask)
{
    if(n<=1)
        return 0;
    int i=n , j=n-1;
    while(i==n)
    {
        i=0;
        j=n-1;
        while(i<=j)
        {
            for(;((array[i]>>mask)&1)!=1 && i<n;++i){}
            for(;j>=0 && ((array[j]>>mask)&1)==1;--j){}
            if(i<j)
                swap(array[i++],array[j--]);
        }
        --mask;
    }

    return i;
};

void BinarySort(int64_t *array,const int64_t n,int64_t mask)
{
    if(mask<0)
        return;
    int part=Partition(array,n,mask);
    if(part>0) BinarySort(array,part,mask);     //part-1
    if(part+1<n) BinarySort(array+part,n-(part),mask);
};




int main() {
    int64_t number;
    cin >> number;
    int64_t mask=31;
    int64_t * array= new int64_t[number];
    for(int64_t i = 0 ; i < number ; ++i)
        cin >> array[i];

    BinarySort(array,number,63);

    for(int64_t i = 0 ; i < number ; ++i)
        cout << array[i]<<" ";

    return 0;
}


