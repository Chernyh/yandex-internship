#include <iostream>
#include "limits.h"
using namespace std;



template <typename T=int, class Comparator = std::less<T>>
class Heap{
public:
        Heap():size(0){};
        explicit Heap(const int *_array,const int& size ): size(size)
        {
           array = new T[size];
           for(int i = 0 ; i < size ; ++i)
               array[i]=_array[i];
            BuildHeap();
        };
        ~Heap()
        {
            size=0;
            delete[]array;
        };
        int ReturnSize()
        {
            return size;
        };
        T ExtractMin()
    {
        T returnable = array[0];
        array[0]=array[size-1];
        --size;
        SiftDown(0);
        return returnable;
    };
        void Insert(const T& element){
        ++size;
        array[size-1]=element;
        SiftUp(size-1);
    };
private:
        void BuildHeap()
        {
            for(int i = size/2-1;i>=0;--i)
                SiftDown(i);
        };
        void SiftDown(const int& i)
        {
            int left=2*i+1;
            int right=2*i+2;
            int smallest=i;
            if(left < size && Compare(array[left],array[i]))
                smallest=left;
            if(right < size && Compare(array[right],array[i]))
                smallest=right;
            if(right < size && left < size && Compare(array[left],array[i])&& Compare(array[right],array[i]) )
                if(Compare(array[left],array[right]))
                    smallest=left;
                else
                    smallest=right;
            if(smallest != i)
            {
                std::swap(array[i],array[smallest]);
                SiftDown(smallest);
            }
        };
        void SiftUp(int i){
            while(i>0)
            {
                int parent = (i-1)/2;
                if(array[i]>=array[parent])
                    return;
                swap(array[i],array[parent]);
                i=parent;
            }
        };
        Comparator Compare;
        int size ;
        T *array ;

};

int TimeForSum(Heap<> heapOfSumm)
{
    int sizeOfHeap=heapOfSumm.ReturnSize();
    int summary=0,first=0,second=0;
    if(sizeOfHeap==1)
        return heapOfSumm.ExtractMin();
    if(sizeOfHeap<=0)
        return 0;
    while(sizeOfHeap>1)
    {
        first=heapOfSumm.ExtractMin();
        second=heapOfSumm.ExtractMin();
        summary+=(first+second);
        heapOfSumm.Insert(first+second);
        --sizeOfHeap;
    }
    return summary;
};

int main() {
    int count=0, *arr;
    cin >> count;
    arr=new int[count];
    for(int i = 0; i< count ; ++i)
        cin >> arr[i];
    Heap<> newHeap(arr,count);
    cout << TimeForSum(newHeap);
    delete[]arr;
    return 0;
}
