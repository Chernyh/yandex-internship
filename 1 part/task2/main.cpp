#include <iostream>
using namespace std;


bool BinSort(const int *mas, int findable,int left, int right)
{
    while(true)
    {
        if(mas[(right+left)/2]>=findable)
        {
            right=(right+left)/2;
        }
        else
        {
            left=(right+left)/2+1;
        }
        if(right-left==1 || right==left)
            return (mas[left]==findable || mas[right]==findable);
    }
};


pair<int,int> Distance(const int *A,const int value,const int LenB)
{

    int left=0;
    int right =1;
    while((A[right]<value) && (right<LenB))
    {
        left=right+1;
        right*=2;
    }
    pair<int,int> answer(left,right);
    return answer;
};


//        left=0;
//        right =1;
//        while((A[right]<B[i]) && (right<LenB))
//        {
//            left=right+1;
//            right*=2;
//        }

void Intersection(const int *A,const int *B, const int& LenA, const int& LenB)
{
    if(A[LenA-1]<B[0])
        return;

    int i=0,left,right;
    pair<int,int> left_right;
    while(i<LenB)
    {
        left_right=Distance(A,B[i],LenB);
        if(left_right.second < LenB)
        {
            if(A[left_right.second]>=B[i])
            {
                if(BinSort(A, B[i] , left_right.first , left_right.second))
                    cout << B[i]<<" ";
            }
        }
        else
        {
            if(A[LenA-1]>=B[i])
                if(BinSort(A, B[i] , left_right.first , LenA-1))
                    cout<<B[i]<<" ";
        }
        ++i;
    }
};

int main() {
    int LenA=0 , LenB=0;
    int *A , *B;
    cin >> LenA >> LenB;
    A=new int[LenA];
    B=new int[LenB];
    for(int i = 0 ; i < LenA ; ++i)
        cin >> A[i];
    for(int i = 0 ; i < LenB ; ++i)
        cin >> B[i];

    Intersection(A,B,LenA,LenB);

    delete[]A;
    delete[]B;
    return 0;
}