#include <iostream>
#include <vector>
#include <cassert>
#include <set>
#include<bits/stdc++.h>


class CListGraph {
public:
    explicit CListGraph(int n);

    ~CListGraph();

    void AddEdge(int from, int to, int weight);

    int MinLengthOfRoad(int from, int to);


private:
    std::vector<std::vector<std::pair<int, int>>> adjacency_lists;
};


CListGraph::CListGraph(int n) : adjacency_lists(n) {

}

CListGraph::~CListGraph() {

}


void CListGraph::AddEdge(int from, int to, int weight) {

    assert(0 <= from && from < adjacency_lists.size());
    assert(0 <= to && to < adjacency_lists.size());
    adjacency_lists[from].push_back(std::make_pair(to, weight));
    adjacency_lists[to].push_back(std::make_pair(from, weight));

}

int CListGraph::MinLengthOfRoad(int from, int to) {


    std::vector<int> relatives(adjacency_lists.size(), 0), length(adjacency_lists.size(), INT_MAX);

    relatives[from] = -1;
    length[from] = 0;
    std::set<std::pair<int, int>> priority_queue;

    priority_queue.insert(std::make_pair(0, from));

    while (!priority_queue.empty()) {
        auto queue_element =priority_queue.begin();
        std::pair<int, int> vertex = *queue_element;
        queue_element = priority_queue.erase(queue_element);


        for (auto &iterator : adjacency_lists[vertex.second]) {
            if (length[iterator.first] == INT_MAX) {

                length[iterator.first] = length[vertex.second] + iterator.second;
                relatives[iterator.first] = vertex.second;

                priority_queue.insert(std::make_pair(length[iterator.first], iterator.first));

            } else if (length[iterator.first] > length[vertex.second] + iterator.second) {

                auto element = priority_queue.find(std::make_pair(length[iterator.first], iterator.first));
                element = priority_queue.erase(element);

                length[iterator.first] = length[vertex.second] + iterator.second;
                relatives[iterator.first] = vertex.second;

                priority_queue.insert(std::make_pair(length[iterator.first], iterator.first));

            }
        }
    }

    return length[to];


}


int main() {


    int vertex = 0, edges = 0;


    std::cin >> vertex >> edges;

    CListGraph listGraph(vertex);

    int i = 0, from = 0, to = 0, weight = 0;

    while (i != edges) {

        std::cin >> from >> to >> weight;

        listGraph.AddEdge(from, to, weight);

        ++i;
    }

    std::cin >> from >> to ;

    std::cout << listGraph.MinLengthOfRoad(from,to);

    return 0;
}