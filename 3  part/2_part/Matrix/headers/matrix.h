//
// Created by znujko on 11.12.2019.
//

#ifndef INC_1_TASK_MATRIX_H
#define INC_1_TASK_MATRIX_H


#include <vector>

struct IGraph {
    virtual ~IGraph() {}
    virtual void AddEdge(int from, int to) = 0;
    virtual int VerticesCount() const  = 0;
    virtual std::vector<int> GetNextVertices(int vertex) const = 0;
    virtual std::vector<int> GetPrevVertices(int vertex) const = 0;
};


class CMatrixGraph : public IGraph {

public :

    explicit CMatrixGraph(int n);

    ~CMatrixGraph() override;

    explicit CMatrixGraph(const IGraph &graph);

    void AddEdge(int from, int to) override;

    int VerticesCount() const override;

    std::vector<int> GetNextVertices(int vertex) const override;

    std::vector<int> GetPrevVertices(int vertex) const override;

    int ShortestWay(int from , int to);

    void printmatrix();

private :

    int **matrixAdjency;

    int verticesCount;

};


#endif //INC_1_TASK_MATRIX_H
