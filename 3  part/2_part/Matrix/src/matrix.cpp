//
// Created by znujko on 11.12.2019.
//

#include <iostream>
#include <cassert>
#include <queue>
#include "matrix.h"

CMatrixGraph::CMatrixGraph(int n) : verticesCount(n) {

    matrixAdjency = new int *[verticesCount];

    for (int i = 0; i < verticesCount; ++i) {
        matrixAdjency[i] = new int[verticesCount];
    }

    for (int i = 0; i < verticesCount; ++i) {
        for (int j = 0; j < verticesCount; ++j) {
            matrixAdjency[i][j] = 0;
        }
    }

}

int CMatrixGraph::VerticesCount() const {
    return verticesCount;
}

CMatrixGraph::~CMatrixGraph() {

    verticesCount = 0;

    for (int i = 0; i < verticesCount; ++i) {
        delete[]matrixAdjency[i];
    }

    delete[]matrixAdjency;

}

CMatrixGraph::CMatrixGraph(const IGraph &graph) : verticesCount(graph.VerticesCount()) {

    matrixAdjency = new int *[verticesCount];

    for (int i = 0; i < verticesCount; ++i) {
        matrixAdjency[i] = new int[verticesCount];
    }

    for (int i = 0; i < verticesCount; ++i) {
        for (int j = 0; j < verticesCount; ++j) {
            matrixAdjency[i][j] = 0;
        }
    }


    for (int i = 0; i < verticesCount; ++i) {
        std::vector<int> edjency = graph.GetNextVertices(i);

        for (int &iterator : edjency) {
            matrixAdjency[i][iterator] = 1;
        }
    }

}

void CMatrixGraph::AddEdge(int from, int to) {

    assert(0 <= from && from < verticesCount);
    assert(0 <= to && to < verticesCount);
    matrixAdjency[from][to] = 1;
    matrixAdjency[to][from] = 1;

}

std::vector<int> CMatrixGraph::GetNextVertices(int vertex) const {

    std::vector<int> retVector;

    for (int j = 0; j < verticesCount; ++j) {
        if (matrixAdjency[vertex][j] != 0)
            retVector.push_back(j);
    }

    return retVector;

}

std::vector<int> CMatrixGraph::GetPrevVertices(int vertex) const {

    std::vector<int> retVector;

    for (int i = 0; i < verticesCount; ++i) {

        if (matrixAdjency[i][vertex] != 0)
            retVector.push_back(i
            );

    }

    return retVector;

}

void CMatrixGraph::printmatrix() {

    for (int i = 0; i < verticesCount; ++i) {
        for (int j = 0; j < verticesCount; ++j) {
            std::cout << matrixAdjency[i][j] << " ";
        }
        std::cout << std::endl;
    }


}

int CMatrixGraph::ShortestWay(int from, int to) {

    int counted = 0;


    std::vector<int> ways(verticesCount, INT8_MAX), relative(verticesCount, INT8_MAX), waysCounted(verticesCount, 0);
    std::queue<int> q;
    q.push(from);

    ways[from] = 0;
    relative[from] = -1;
    waysCounted[from] = 1;

    while (!q.empty()) {
        int vertex = q.front();
        q.pop();

        for (int j = 0; j < verticesCount; ++j) {
            if (matrixAdjency[vertex][j] != 0) {


                if (ways[j] == ways[vertex] + 1) {
                    waysCounted[j] += waysCounted[vertex];
                }


                if (ways[j] > ways[vertex] + 1) {

                    waysCounted[j] = 0;
                    waysCounted[j] += waysCounted[vertex];

                    ways[j] = ways[vertex] + 1;
                    relative[j] = vertex;
                    q.push(j);
                }
            }
        }
    }

    return waysCounted[to];


}









