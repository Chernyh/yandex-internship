#include <iostream>
#include <cassert>
#include <queue>
#include <vector>
#include <bits/stdc++.h>

class CListGraph {
public:
    explicit CListGraph(int n);

    ~CListGraph();


    void AddEdge(int from, int to);

    int ShortestWay(int from, int to);


private:
    std::vector< std::vector<int> > adjacency_lists;
};


CListGraph::CListGraph(int n) : adjacency_lists(n) {

}

CListGraph::~CListGraph() {

}


void CListGraph::AddEdge(int from, int to) {

    assert(0 <= from && from < adjacency_lists.size());
    assert(0 <= to && to < adjacency_lists.size());
    adjacency_lists[from].push_back(to);
    adjacency_lists[to].push_back(from);

}


int CListGraph::ShortestWay(int from, int to) {

    int counted = 0;


    std::vector<int> ways(adjacency_lists.size(), INT_MAX), relative(adjacency_lists.size(), INT_MAX), waysCounted(
            adjacency_lists.size(), 0);
    std::queue<int> q;
    q.push(from);

    ways[from] = 0;
    relative[from] = -1;
    waysCounted[from] = 1;

    while (!q.empty()) {
        int vertex = q.front();
        q.pop();

        for (auto &iterator : adjacency_lists[vertex]) {

            if (ways[iterator] == ways[vertex] + 1) {
                waysCounted[iterator] += waysCounted[vertex];
            }


            if (ways[iterator] > ways[vertex] + 1) {

                waysCounted[iterator] = 0;
                waysCounted[iterator] += waysCounted[vertex];

                ways[iterator] = ways[vertex] + 1;
                relative[iterator] = vertex;
                q.push(iterator);
            }
        }
    }

    return waysCounted[to];


}


using namespace std;

int main() {


    int vertex = 0, edge = 0;

    cin >> vertex >> edge;

    CListGraph matrixGraph(vertex);

    int i = 0;
    int from = 0, to = 0;
    while (i != edge) {
        cin >> from >> to;

        matrixGraph.AddEdge(from, to);

        ++i;

    }
    cin >> from >> to;


    cout << matrixGraph.ShortestWay(from, to);

    return 0;
}