#include <iostream>
#include "matrix.h"
#include "set.h"
#include "arc.h"

void BFS(const IGraph &graph, int vertex, std::vector<bool> &visited, const std::function<void(int)> &func)
{
    std::queue<int> qu;
    qu.push(vertex);
    visited[vertex] = true;

    while (!qu.empty())
    {
        int current_vertex = qu.front();
        qu.pop();

        func(current_vertex);

        for (int next_vertex: graph.GetNextVertices(current_vertex))
        {
            if (!visited[next_vertex])
            {
                qu.push(next_vertex);
                visited[next_vertex] = true;
            }
        }
    }
}

void mainBFS(const IGraph &graph, const std::function<void(int)> &func)
{
    std::vector<bool> visited(graph.VerticesCount(), false);

    for (int vertex = 0; vertex < graph.VerticesCount(); vertex++)
    {
        if (!visited[vertex])
        {
            BFS(graph, vertex, visited, func);
        }
    }
}

void topological_sort_internal(const IGraph &graph, int vertex, std::vector<bool> &visited, std::deque<int> &sorted)
{
    visited[vertex] = true;
    for (int next_vertex: graph.GetNextVertices(vertex))
    {
        if (!visited[next_vertex])
        {
            topological_sort_internal(graph, next_vertex, visited, sorted);
        }
    }
    sorted.push_front(vertex);
}

std::deque<int> topological_sort(const IGraph &graph)
{
    std::vector<bool> visited(graph.VerticesCount(), false);
    std::deque<int> sorted;

    for (int vertex = 0; vertex < graph.VerticesCount(); vertex++)
    {
        if (!visited[vertex])
        {
            topological_sort_internal(graph, vertex, visited, sorted);
        }
    }

    return sorted;
}

void print(const IGraph &graph)
{
    mainBFS(graph, [](int vertex) {std::cout << vertex << " ";});
    std::cout << std::endl;

    for (int i : topological_sort(graph))
        std::cout << i << " ";
    std::cout << std::endl;
}






using namespace std;
int main() {

    CListGraph graph(11);
    graph.AddEdge(0, 1);
    graph.AddEdge(0, 5);
    graph.AddEdge(1, 2);
    graph.AddEdge(1, 3);
    graph.AddEdge(1, 5);
    graph.AddEdge(1, 6);
    graph.AddEdge(3, 2);
    graph.AddEdge(3, 4);
    graph.AddEdge(3, 6);
    graph.AddEdge(5, 4);
    graph.AddEdge(5, 6);
    graph.AddEdge(6, 4);

    graph.AddEdge(7, 8);
    graph.AddEdge(7, 9);

    graph.AddEdge(10, 0);

    std::cout << "graph:" << std::endl;
    print(graph);





    CMatrixGraph graphMatrix(graph);

    std::cout << std::endl <<"graphMatrix:" << std::endl;

    print(graphMatrix);

    CSetGraph graphSet(graphMatrix);

    std::cout << std::endl <<"graphSet:" << std::endl;

    print(graphSet);


    CArcGraph graphArc(graphSet);

    std::cout << std::endl <<"graphArc:" << std::endl;

    print(graphArc);


    return 0;
}



