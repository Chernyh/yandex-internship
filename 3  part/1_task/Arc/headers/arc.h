//
// Created by znujko on 11.12.2019.
//

#ifndef INC_1_TASK_ARC_H
#define INC_1_TASK_ARC_H


#include "list.h"

class CArcGraph : public IGraph {

public :

    explicit CArcGraph(int n);

    ~CArcGraph() override;

    explicit CArcGraph(const IGraph &graph);

    void AddEdge(int from, int to) override;

    int VerticesCount() const override;

    std::vector<int> GetNextVertices(int vertex) const override;

    std::vector<int> GetPrevVertices(int vertex) const override;


private :

    std::vector<std::pair<int,int>> pairVector;

    int verticesCount;
};







#endif //INC_1_TASK_ARC_H
