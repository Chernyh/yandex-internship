//
// Created by znujko on 11.12.2019.
//

#include "arc.h"

CArcGraph::CArcGraph(int n) : verticesCount(n) {

}

CArcGraph::~CArcGraph() {

}

CArcGraph::CArcGraph(const IGraph &graph) : verticesCount(graph.VerticesCount()) {

    for(int i = 0 ; i < verticesCount ; ++i)
    {
        std::vector<int> edjency = graph.GetNextVertices(i);

        for(auto &iterator : edjency)
        {
            pairVector.push_back(std::make_pair(i,iterator));
        }
    }

}

void CArcGraph::AddEdge(int from, int to) {

    pairVector.push_back(std::make_pair(from,to));

}

int CArcGraph::VerticesCount() const {
    return verticesCount;
}

std::vector<int> CArcGraph::GetNextVertices(int vertex) const {

    std::vector<int> retVector;

    bool isItem = false;
    bool wasItem = false;

    for(auto &iterator : pairVector)
    {
        if(iterator.first == vertex)
        {
            isItem = true;

            wasItem = true;

            retVector.push_back(iterator.second);

        }

        if(iterator.first != vertex)
        {
            isItem = false;
        }

        if(!isItem && wasItem)
        {
            return retVector;
        }
    }

    return retVector;

}

std::vector<int> CArcGraph::GetPrevVertices(int vertex) const {

    std::vector<int> retVector;


    for(auto &iterator : pairVector)
    {
        if(iterator.second == vertex)
        {

            retVector.push_back(iterator.first);

        }

    }

    return retVector;




}




