//
// Created by znujko on 11.12.2019.
//

#ifndef INC_1_TASK_LIST_H
#define INC_1_TASK_LIST_H



#include <iostream>
#include <vector>
#include <cassert>
#include <queue>
#include <functional>

struct IGraph {
    virtual ~IGraph() {}
    virtual void AddEdge(int from, int to) = 0;
    virtual int VerticesCount() const  = 0;
    virtual std::vector<int> GetNextVertices(int vertex) const = 0;
    virtual std::vector<int> GetPrevVertices(int vertex) const = 0;
};

class CListGraph: public IGraph
{
public:
    explicit CListGraph(int n) ;

    ~CListGraph() override;

    explicit CListGraph(const IGraph &graph) ;

    void AddEdge(int from, int to) override;


    int VerticesCount() const override;


    std::vector<int> GetNextVertices(int vertex) const override;


    std::vector<int> GetPrevVertices(int vertex) const override;


private:
    std::vector<std::vector<int>> adjacency_lists;
};




#endif //INC_1_TASK_LIST_H
