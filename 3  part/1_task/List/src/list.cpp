//
// Created by znujko on 11.12.2019.
//

#include "list.h"

CListGraph::CListGraph(int n) : adjacency_lists(n) {

}

CListGraph::~CListGraph() {

}

CListGraph::CListGraph(const IGraph &graph) : adjacency_lists(graph.VerticesCount()) {
    for (int i = 0; i < adjacency_lists.size(); i++) {
        adjacency_lists[i] = graph.GetNextVertices(i);
    }
}

void CListGraph::AddEdge(int from, int to) {


    assert(0 <= from && from < adjacency_lists.size());
    assert(0 <= to && to < adjacency_lists.size());
    adjacency_lists[from].push_back(to);


}

std::vector<int> CListGraph::GetNextVertices(int vertex) const {

    assert(0 <= vertex && vertex < adjacency_lists.size());
    return adjacency_lists[vertex];

}

int CListGraph::VerticesCount() const {

    return (int) adjacency_lists.size();

}

std::vector<int> CListGraph::GetPrevVertices(int vertex) const {

    std::vector<int> prev_vertices;

    for (int from = 0; from < adjacency_lists.size(); from++) {
        for (int to: adjacency_lists[from]) {
            if (to == vertex) {
                prev_vertices.push_back(from);
            }
        }
    }
    return prev_vertices ;

}
