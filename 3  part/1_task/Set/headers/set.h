//
// Created by znujko on 11.12.2019.
//

#ifndef INC_1_TASK_SET_H
#define INC_1_TASK_SET_H

#include <set>
#include "list.h"

class CSetGraph : public IGraph {

public :

    explicit CSetGraph(int n);

    ~CSetGraph() override;

    explicit CSetGraph(const IGraph &graph);

    void AddEdge(int from, int to) override;

    int VerticesCount() const override;

    std::vector<int> GetNextVertices(int vertex) const override;

    std::vector<int> GetPrevVertices(int vertex) const override;


private :

    std::set<int> *setMatrix;

    int verticesCount;
};








#endif //INC_1_TASK_SET_H
