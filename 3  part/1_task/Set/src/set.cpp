//
// Created by znujko on 11.12.2019.
//



#include "set.h"

CSetGraph::CSetGraph(int n) : verticesCount(n) {

    setMatrix = new std::set<int>[verticesCount];

}

CSetGraph::~CSetGraph() {

    for (int i = 0; i < verticesCount; ++i)
        setMatrix[i].clear();

    delete[]setMatrix;

}

CSetGraph::CSetGraph(const IGraph &graph) : verticesCount(graph.VerticesCount()){

    setMatrix = new std::set<int>[verticesCount];

    for(int i = 0 ; i < verticesCount ; ++i)
    {
        std::vector<int> edjency = graph.GetNextVertices(i);

        for(auto &iterator : edjency)
        {
            setMatrix[i].insert(iterator);
        }
    }

}

void CSetGraph::AddEdge(int from, int to) {

    setMatrix[from].insert(to);

}

int CSetGraph::VerticesCount() const {
    return verticesCount;
}

std::vector<int> CSetGraph::GetNextVertices(int vertex) const {

    std::vector<int> retVector;

    for(int iterator : setMatrix[vertex])
    {
        retVector.push_back(iterator);
    }
    return retVector;

}

std::vector<int> CSetGraph::GetPrevVertices(int vertex) const {


    std::vector<int> retVector;

    for(int i = 0 ; i < verticesCount ; ++i)
    {
        auto iterator = setMatrix[i].find(vertex);
        if(iterator != setMatrix[i].end())
        {
            retVector.push_back(i);
        }
    }

    return retVector;


}





