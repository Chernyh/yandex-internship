//
// Created by znujko on 11.12.2019.
//

#ifndef INC_1_TASK_MATRIX_H
#define INC_1_TASK_MATRIX_H


#include "list.h"


class CMatrixGraph : public IGraph {

public :

    explicit CMatrixGraph(int n);

    ~CMatrixGraph() override;

    explicit CMatrixGraph(const IGraph &graph);

    void AddEdge(int from, int to) override;

    int VerticesCount() const override;

    std::vector<int> GetNextVertices(int vertex) const override;

    std::vector<int> GetPrevVertices(int vertex) const override;

    void printmatrix();

private :

    int **matrixAdjency;

    int verticesCount;

};


#endif //INC_1_TASK_MATRIX_H
