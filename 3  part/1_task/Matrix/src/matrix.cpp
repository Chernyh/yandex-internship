//
// Created by znujko on 11.12.2019.
//

#include "matrix.h"

CMatrixGraph::CMatrixGraph(int n) : verticesCount(n) {

    matrixAdjency = new int *[verticesCount];

    for (int i = 0; i < verticesCount; ++i) {
        matrixAdjency[i] = new int[verticesCount];
    }

    for(int i = 0 ; i <verticesCount ; ++i)
    {
        for(int j= 0 ; j < verticesCount ; ++j)
        {
            matrixAdjency[i][j]=0;
        }
    }

}

int CMatrixGraph::VerticesCount() const {
    return verticesCount;
}

CMatrixGraph::~CMatrixGraph() {

    verticesCount = 0;

    for (int i = 0; i < verticesCount; ++i) {
        delete[]matrixAdjency[i];
    }

    delete[]matrixAdjency;

}

CMatrixGraph::CMatrixGraph(const IGraph &graph) : verticesCount(graph.VerticesCount()) {

    matrixAdjency = new int *[verticesCount];

    for (int i = 0; i < verticesCount; ++i) {
        matrixAdjency[i] = new int[verticesCount];
    }

    for(int i = 0 ; i <verticesCount ; ++i)
    {
        for(int j= 0 ; j < verticesCount ; ++j)
        {
            matrixAdjency[i][j]=0;
        }
    }


    for (int i = 0; i < verticesCount; ++i) {
        std::vector<int> edjency = graph.GetNextVertices(i);

        for (int &iterator : edjency) {
            matrixAdjency[i][iterator] = 1;
        }
    }

}

void CMatrixGraph::AddEdge(int from, int to) {

    assert(0 <= from && from < verticesCount);
    assert(0 <= to && to < verticesCount);
    matrixAdjency[from][to] = 1;

}

std::vector<int> CMatrixGraph::GetNextVertices(int vertex) const {

    std::vector<int> retVector;

    for (int j = 0; j < verticesCount; ++j) {
        if (matrixAdjency[vertex][j] != 0)
            retVector.push_back(j);
    }

    return retVector;

}

std::vector<int> CMatrixGraph::GetPrevVertices(int vertex) const {

    std::vector<int> retVector;

    for (int i = 0; i < verticesCount; ++i) {

        if (matrixAdjency[i][vertex] != 0)
            retVector.push_back(i
            );

    }

    return retVector;

}

void CMatrixGraph::printmatrix() {

    for (int i = 0 ; i <verticesCount ; ++i)
    {
        for(int j = 0 ; j < verticesCount ; ++j)
        {
           std::cout << matrixAdjency[i][j] << " ";
        }
        std::cout<<std::endl;
    }


}









