#include <iostream>
#include <vector>


template<typename T>
struct Node {

    Node(const T &_key) : key(_key), height(1), left(nullptr), right(nullptr), countedLeft(0), countedRight(0) {};
    T key;
    unsigned char height;
    int countedLeft;
    int countedRight;
    Node *left;
    Node *right;
};


template<typename T>
class AvlTree {

public:

    AvlTree() : head(nullptr) {

    };

    ~AvlTree() {
        DestroyTree(head);
    };

    void Delete(const T &deleteItem) {
        head = DeleteItem(head, deleteItem);
    };

    void Add(const T &addItem) {
        head = AddItem(head, addItem);
    };


    int64_t getK(const T &getStatisticItem) {
        return OrderStatistic(head, getStatisticItem);
    };


private:

    Node<T> *AddItem(Node<T> *head, const T &addItem) {
        if (!(head)) {
            head = new Node<T>(addItem);
            return head;
        }

        if (addItem < (head)->key) {
            (head)->left = AddItem(((head)->left), addItem);
        } else {
            (head)->right = AddItem(((head)->right), addItem);
        }


        return FixHead(head);
    };

    Node<T> *DeleteItem(Node<T> *tmp, const T &deleteItem) {
        if (!tmp) {
            return nullptr;
        }
        if (deleteItem < tmp->key) {
            tmp->left = DeleteItem(tmp->left, deleteItem);
        }
        if (deleteItem > tmp->key) {
            tmp->right = DeleteItem(tmp->right, deleteItem);
        }
        if (tmp->key == deleteItem) {
            Node<T> *left = tmp->left;
            Node<T> *right = tmp->right;

            delete tmp;

            if (!right)
                return left;

            Node<T> *min = findMin(right);
            min->right = removeMin(right);
            min->left = left;

            return FixHead(min);
        }
        return FixHead(tmp);
    };

    int64_t OrderStatistic(Node<T> *tmp, int64_t getStatisticItem) {
        if (!tmp) {
            return 0;
        }


        if (tmp->countedLeft == getStatisticItem) {
            return tmp->key;
        }
        if (tmp->countedLeft > getStatisticItem) {
            return OrderStatistic(tmp->left, getStatisticItem);
        }
        if (tmp->countedLeft < getStatisticItem) {
            return OrderStatistic(tmp->right, getStatisticItem - tmp->countedLeft - 1);
        }

    }

    Node<T> *findMin(Node<T> *node) {
        while (node->left)
            node = node->left;
        return node;
    }

    Node<T> *removeMin(Node<T> *node) {
        if (node->left == nullptr)
            return node->right;
        node->left = removeMin(node->left);
        return FixHead(node);
    }

    Node<T> *FixHead(Node<T> *head) {
        FixHeight(head);

        int balanceCoef = IsBalanced(head);

        if (balanceCoef == 2) {
            if (IsBalanced(head->right) < 0)
                head->right = rightRotate(head->right);
            return leftRotate(head);
        }
        if (balanceCoef == -2) {
            if (IsBalanced(head->left) > 0)
                head->left = leftRotate(head->left);
            return rightRotate(head);
        }
        return head;
    };

    int IsBalanced(Node<T> *head) {

        return head ? (int) (GetHeight(head->right) - GetHeight(head->left)) : 0;
    }

    void FixHeight(Node<T> *head) {
        head->height = std::max(GetHeight(head->left), GetHeight(head->right)) + 1;
        head->countedRight = GetCounted(head->right);
        head->countedLeft = GetCounted(head->left);
    };

    int GetCounted(Node<T> *head) {
        return head ? head->countedLeft + head->countedRight + 1 : 0;
    };

    unsigned char GetHeight(Node<T> *head) {
        return head ? head->height : 0;
    }

    Node<T> *rightRotate(Node<T> *node) {
        Node <T> *tmp = node->left;
        node->left = tmp->right;
        tmp->right = node;

        FixHeight(node);
        FixHeight(tmp);
        return (tmp);
    };

    Node<T> *leftRotate(Node<T> *node) {
        Node <T> *tmp = node->right;
        node->right = tmp->left;
        tmp->left = node;

        FixHeight(node);
        FixHeight(tmp);
        return (tmp);
    };

    void DestroyTree(Node<T> *tmp) {
        if (tmp) {
            DestroyTree(tmp->left);
            DestroyTree(tmp->right);
            delete tmp;
        }
    }

    Node<T> *head;

};

int main() {
    int count = 0, i = 0;
    int64_t command = 0, statistic = 0;
    std::cin >> count;
    std::vector<int64_t> results;

    AvlTree<int64_t> tree;

    while (i < count) {
        //std :: cout << i << std :: endl;
        ++i;
        std::cin >> command >> statistic;
        if (command >= 0) {
            tree.Add(command);
            std::cout << tree.getK(statistic) << std::endl;
            results.push_back(tree.getK(statistic));
        }
        if (command < 0) {
            tree.Delete(abs(command));
            std::cout << tree.getK(statistic) << std::endl;
            results.push_back(tree.getK(statistic));
        }
    }


    // std :: cout << std :: endl ;

   // for (int i = 0; i < count; ++i) {
   //     std::cout << results[i] << std::endl;
   // }


    return 0;


}