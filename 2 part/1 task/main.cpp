#include <iostream>
#include <vector>
using namespace std;

size_t DEFAULT_SIZE=8;
size_t MAXALPHA=3;



size_t Hash(const std::string _string,size_t size)
{
    size_t hash=0;
    for(auto i = 0 ; i < _string.size(); ++i)
    {
        hash=(hash*71+_string[i])%size;
    }
    return hash;
};


template <typename T>
struct  HashNode
{
    HashNode(): next(nullptr) {};
    HashNode(const T &key , HashNode<T> *next) : key(key) , next(next) {};
    T key;
    HashNode<T> *next;

};




template <typename T>
class HashTable
{
public :
        HashTable(size_t tableSize=DEFAULT_SIZE) : table(tableSize,nullptr),size(tableSize){};
        ~HashTable()
        {
            for(size_t i = 0 ; i < table.size() ; ++i)
            {
                HashNode<T> *node = table[i];
                while(node != nullptr)
                {

                    HashNode<T> *tmpNext = node->next;
                    delete node;
                    node=tmpNext;
                }
            }
        };

        bool Add(const T &key)
        {

            if(size > table.size()*MAXALPHA)
                grow();

            size_t hash= Hash(key,table.size());
            HashNode<T> *node = table[hash];
            while(node !=nullptr && node->key !=key)
            {
                node = node->next;
            }

            if (node != nullptr)
                return false;
            table[hash]=new HashNode<T>(key,table[hash]);
            ++size;
            return true;

        };
        bool Delete(const T &key)
        {
            size_t hash= Hash(key,table.size());
            HashNode<T> *prevNode = nullptr;
            HashNode<T> *node = table[hash];
            while(node !=nullptr && node->key !=key)
            {
                prevNode = node;
                node = node->next;
            }

            if(node == nullptr)
                return false;

            if(prevNode == nullptr)
            {
                table[hash]=node->next;
            }
            else
            {
                prevNode->next = node->next;
            }

            --size;
            delete node;
            return true;

        };
        bool Has(const T &key)
        {
            size_t hash= Hash(key,table.size());
            HashNode<T> *node = table[hash];
            while(node !=nullptr && node->key !=key)
            {
                node = node->next;
            }


            return node != nullptr;
        };

private:

        void grow()
        {
            vector<HashNode<T>*> newTable(table.size()*2,nullptr);

            for(size_t i = 0 ; i < table.size() ; ++i)
            {
                HashNode<T> *node = table[i];
                while(node != nullptr)
                {
                    size_t newHash= Hash(node->key,newTable.size());
                    HashNode<T> *tmpNext = node->next;
                    node->next= newTable[newHash];
                    newTable[newHash]=node;
                    node=tmpNext;
                }
            }

            newTable = std::move(table);


        };


        size_t size;
        vector<HashNode<T>*> table;

};


int main() {
    HashTable<std::string> table;
    char op;
    std::string key;

    while(std:: cin >> op >> key)
    {
        switch(op)
        {
            case '?':
            {
                 std::cout << (table.Has(key) ? "OK" : "FAIL") << std::endl;
                 break;
            }
            case '+':
            {
                std::cout << (table.Add(key) ? "OK" : "FAIL") << std::endl;
                break;
            }

            case '-':
            {
                std::cout << (table.Delete(key) ? "OK" : "FAIL") << std::endl;
                break;
            }
        }
    }



    return 0;
}