#include <iostream>
#include <stack>

using namespace std;

template<typename T>
class DecartTree {
public :


    DecartTree() : head(nullptr) {};

    ~DecartTree() {

        stack<KeyValue *> _stack;

        KeyValue *lastVisitedNode = nullptr;
        KeyValue *node = head;

        while (!_stack.empty() || node != nullptr) {
            if (node != nullptr) {
                _stack.push(node);
                node = node->left;
            } else {
                KeyValue *peekNode = _stack.top();

                if (peekNode->right != nullptr && lastVisitedNode != peekNode->right) {
                    node = peekNode->right;
                } else {
                    delete[]peekNode;
                    lastVisitedNode = _stack.top();
                    _stack.pop();
                }

            }
        }

    };

    struct KeyValue {
        KeyValue(const T &_key, const T &_priority) : key(_key), priority(_priority), right(nullptr), left(nullptr) {};


        KeyValue &operator=(const KeyValue &leftOperand) {
            this->priority = leftOperand.priority;
            this->key = leftOperand.key;
            this->right = leftOperand.right;
            this->left = leftOperand.left;
        };

        T key;
        T priority;
        KeyValue *right;
        KeyValue *left;
    };


    void AddElement(const T &_key, const T &_priority) {
        if (head == nullptr) {
            head = new KeyValue(_key, _priority);
        } else {
            KeyValue *itemToAdd = new KeyValue(_key, _priority);
            Add(itemToAdd, head);
        }


    };

    int64_t MaxHeight() {
        return countHeight(head, 0);
    };


private :

    KeyValue *head;

    void Split(KeyValue *currentNode, const T &key, KeyValue *&left, KeyValue *&right) {

        if (currentNode == nullptr) {
            right = nullptr;
            left = nullptr;
        } else if (currentNode->key <= key) {
            Split(currentNode->right, key, currentNode->right, right);
            left = currentNode;
        } else {
            Split(currentNode->left, key, left, currentNode->left);
            right = currentNode;
        }

    };

    void Add(KeyValue *addPlace, KeyValue *&_head) {

        if (_head == nullptr) {
            _head = addPlace;
        } else if (addPlace->priority > _head->priority) {
            Split(_head, addPlace->key, addPlace->left, addPlace->right);
            _head = addPlace;
        } else {
            Add(addPlace, _head->key > addPlace->key ? _head->left : _head->right);
        }
    };

    int64_t countHeight(KeyValue *head, int64_t height) {
        if (head == nullptr) {
            return height;
        }
        return max(countHeight(head->left, (height + 1)), countHeight(head->right, (height + 1)));
    }

};


template<typename T, class Comparator = std::less<T> >
class BinaryTree {
    struct Node {

        Node() : count(0), left(nullptr), right(nullptr) {};

        Node(T init) : count(init), left(nullptr), right(nullptr) {};

        T count;
        Node *left;
        Node *right;
    };

public :

    BinaryTree() : head(nullptr) {};

    ~BinaryTree() {

        stack<Node *> _stack;

        Node *lastVisitedNode = nullptr;
        Node *node = head;

        while (!_stack.empty() || node != nullptr) {
            if (node != nullptr) {
                _stack.push(node);
                node = node->left;
            } else {
                Node *peekNode = _stack.top();

                if (peekNode->right != nullptr && lastVisitedNode != peekNode->right) {
                    node = peekNode->right;
                } else {
                    delete[]peekNode;
                    lastVisitedNode = _stack.top();
                    _stack.pop();
                }

            }
        }

    };

    void add(const T &toAdd) {
        if (head == nullptr)
            head = new Node(toAdd);
        else {
            Node *tmp = head;
            while ((tmp->left != nullptr && comparator(toAdd, tmp->count)) ||
                   (tmp->right != nullptr && !comparator(toAdd, tmp->count))) {
                if (comparator(toAdd, tmp->count)) {
                    tmp = tmp->left;
                } else {
                    tmp = tmp->right;
                }
            }

            if (comparator(toAdd, tmp->count)) {
                tmp->left = new Node(toAdd);
            } else {
                tmp->right = new Node(toAdd);
            }
        }

    };

    int64_t MaxHeight() {
        return countHeight(head, 0);
    };


private :

    Comparator comparator;

    Node *head;

    int64_t countHeight(Node *head, int64_t height) {
        if (head == nullptr) {
            return height;
        }
        return max(countHeight(head->left, (height + 1)), countHeight(head->right, (height + 1)));
    };

};


int main() {

    int count = 0, i = 0;
    int64_t key = 0, priority = 0;

    DecartTree<int64_t> decartTree;
    BinaryTree<int64_t> binaryTree;

    cin >> count;

    while (i < count) {
        ++i;

        cin >> key >> priority;

        decartTree.AddElement(key, priority);
        binaryTree.add(key);
    }


    cout << abs(decartTree.MaxHeight() - binaryTree.MaxHeight());


    return 0;
}