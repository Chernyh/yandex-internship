#include <iostream>
#include <vector>

using namespace std;

#define startLength 8
#define multiPlied 3
#define fillFactor 0.75




size_t Hash(std::string toHash,int sizeOfTable)
{
    size_t hash=0;
    for(char i : toHash)
    {
        hash = (hash*multiPlied+i) % sizeOfTable;
    }

    return hash;
};



class HashTable
{

public :
    vector<string> hashTable;

    HashTable(size_t (*hashFunc)(string,int),size_t sizeOf=startLength):sizeOfHash(0),sizeOfTable(sizeOf),hashTable(sizeOf,"NILL")
    {
        hashResult=hashFunc;
    };
    ~HashTable()
    {
        sizeOfHash=0;
        hashResult=nullptr;
    };

    bool Has(string toFind)
    {

        size_t findPos = Probe(toFind,sizeOfTable);
        int i = 0 ;

        while(hashTable[findPos] != toFind && hashTable[findPos] != "NILL" && i < sizeOfTable)
        {
            ++i;
            findPos=(findPos+i)%sizeOfTable;
        }


        return hashTable[findPos] == toFind;

    };

    bool Add(string toAdd)
    {
        if(Has(toAdd))
            return false;


        if(sizeOfHash >= (int)(fillFactor*sizeOfTable))
        {
            grow();
        }


        int i = 0;
        int addPos = Probe(toAdd,sizeOfTable);

        while(hashTable[addPos]!="NILL" && hashTable[addPos]!="DEL")
        {
            ++i;
            addPos=(addPos+i)%sizeOfTable;
        }

        hashTable[addPos]=toAdd;
        ++sizeOfHash;
        return true;
    }

    bool Delete(string toDelete)
    {

        size_t deletePos = Probe(toDelete,sizeOfTable);
        int i = 0;

        while(hashTable[deletePos]!=toDelete && hashTable[deletePos]!="NILL" && i < sizeOfTable)
        {
            ++i;
            deletePos=(deletePos+i)%sizeOfTable;
        }

        if(hashTable[deletePos]==toDelete)
        {
            hashTable[deletePos]="DEL";
            --sizeOfHash;
            return true;
        }
        else
        {
            return false;
        }
    };

private :
    size_t Probe(string toHash,size_t size)
    {
        return hashResult(toHash,size);
    };

    void grow()
    {
        vector<string> newTable(sizeOfTable*2,"NILL");
        size_t i = 0 , counted = 0 , addToNew = 0;
        while(i<sizeOfTable && counted < sizeOfHash)
        {
            if(hashTable[i] != "NILL" && hashTable[i] != "DEL")
            {
                size_t addToNew = Probe(hashTable[i],newTable.size());
                int j = 0;

                while(newTable[addToNew]!="NILL" && newTable[addToNew]!="DEL")
                {
                    ++j;
                    addToNew=(addToNew+j)%newTable.size();
                }

                newTable[addToNew]=hashTable[i];
                ++counted;
            }

            ++i;
        }
        sizeOfTable*=2;
        hashTable = move(newTable);
    }


    size_t sizeOfHash, sizeOfTable;
    size_t (*hashResult)(string,int);

};




int main() {
    HashTable table(Hash);
    char op;
    string key;
    vector<string> answers;
    int64_t size = 0 ;

    while(cin >> op >> key)
    {
        switch(op)
        {
            case '?':
            {
                table.Has(key) ? answers.emplace_back("OK") : answers.emplace_back("FAIL") ;
                //cout << answers[size] << endl;
                //table.Print();
                ++size;
                break;
            }
            case '+':
            {
                table.Add(key) ? answers.emplace_back("OK") : answers.emplace_back("FAIL");
                //cout << answers[size] << endl;
                //table.Print();
                ++size;
                break;
            }

            case '-':
            {
                table.Delete(key) ? answers.emplace_back("OK") : answers.emplace_back("FAIL");
                //cout << answers[size] << endl;
                //table.Print();
                ++size;
                break;
            }


        }
    }


    for(int64_t i = 0 ; i < size ; ++i)
        cout << answers[i] << endl;
    return 0;
}