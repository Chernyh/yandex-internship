#include <iostream>
#include <stack>

using namespace std;


template<typename T, class Comparator = std::less<T> >
class BinaryTree {
    struct Node {

        Node() : count(0), left(nullptr), right(nullptr) {};

        Node(T init) : count(init), left(nullptr), right(nullptr) {};

        T count;
        Node *left;
        Node *right;
    };

public :

    BinaryTree() : head(nullptr) {};

    void add(const T &toAdd) {
        if (head == nullptr)
            head = new Node(toAdd);
        else {
            Node *tmp = head;
            while ((tmp->left != nullptr && comparator(toAdd,tmp->count)) || (tmp->right != nullptr && !comparator(toAdd,tmp->count))) {
                if (comparator(toAdd,tmp->count)) {
                    tmp = tmp->left;
                } else {
                    tmp = tmp->right;
                }
            }

            if (comparator(toAdd,tmp->count)) {
                tmp->left = new Node(toAdd);
            } else {
                tmp->right = new Node(toAdd);
            }
        }

    };

    void Print() {

        stack<Node *> _stack;

        Node *lastVisitedNode = nullptr;
        Node *node = head;

        while (!_stack.empty() || node != nullptr) {
            if (node != nullptr) {
                _stack.push(node);
                node = node->left;
            } else {
                Node *peekNode = _stack.top();

                if (peekNode->right != nullptr && lastVisitedNode != peekNode->right) {
                    node = peekNode->right;
                } else {
                    cout << peekNode->count << " ";
                    lastVisitedNode = _stack.top();
                    _stack.pop();
                }

            }
        }
    };


private :

    Comparator comparator;

    Node *head;

};


int main() {
    int i = 0, numbers = 0;
    int64_t variable = 0;
    cin >> numbers;

    BinaryTree<int64_t> binaryTree;

    while (i < numbers) {
        cin >> variable;
        binaryTree.add(variable);
        ++i;
    }
    binaryTree.Print();

}

