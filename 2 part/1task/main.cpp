#include <iostream>
#include <vector>
using namespace std;

#define defaultSize 8096
#define fillFactor 0.75
#define goldenSection 3 // простое

size_t Hash(const string& stringToHash,size_t sizeOfTable)
{
    size_t hash=0;
    for(char i : stringToHash)
    {
       hash = (int)(hash*goldenSection+i) % sizeOfTable;
    }

    return hash;
};

class HashTable
{
public :
    HashTable(size_t tableSize=defaultSize) : table(tableSize,"NILL"),size(0){};
    ~HashTable()
    {     size = 0;    };

    bool Add(const string &toAdd)
    {

        if(size >= (int)(table.size()*fillFactor))
        {
            //cout << "resize " << size << " " << (int)(table.size()*fillFactor) << endl;
            grow();
        }


        size_t hashPosition=iProbe(table,toAdd);
        //cout << "position is :" << Hash(toAdd,table.size()) << endl;
        if(table[hashPosition] == toAdd)
            return false;

        table[hashPosition]=toAdd;
        ++size;
        return true;

    };

    bool Delete(const string &toDelete)
    {
        size_t posToDelete = iProbe(table,toDelete);

        if(table[posToDelete] == toDelete)
        {
            table[posToDelete] = "DEL";
            --size;
            return true;
        }

        return false;



    };

    bool Has(const string &toSearch)
    {

        size_t posToCheck = iProbe(table,toSearch);

        return table[posToCheck] == toSearch;

    };

    void Print()
    {
        for (const auto & i : table)
            cout << i << " ";
        cout << endl;
    }


private:

    void grow()
    {
        vector<string> newTable(table.size()*2,"NILL");
        size_t i = 0 , counted = 0 ;
        while(i<table.size() && counted < size)
        {
            if(table[i] != "NILL" && table[i] != "DEL")
            {
                newTable[iProbe(newTable,table[i])] = table[i];
                ++counted;
            }
            ++i;

        }

        table = move(newTable);


    };

    static size_t iProbe(vector<string> _table,const string& toPlace)
    {
        size_t sizeOfTable = _table.size();
        size_t position=Hash(toPlace,sizeOfTable);
        size_t visited = 0;
        //cout << " Hash |" << toPlace << ":" << position << "|" << endl;
        int ited=0;
        while(!(_table[position] =="NILL" || _table[position] == toPlace || _table[position] == "DEL") && visited != sizeOfTable)  // вынести за функцию
        {
            ++ited;
            position+=ited;
            position%=sizeOfTable;
            ++visited;
        }
        return position;
    }

    size_t size;
    vector<string> table;


};

int main() {
    HashTable table;
    char op;
    string key;
    vector<string> answers;
    int64_t size = 0 ;

    while(cin >> op >> key)
    {
        switch(op)
        {
            case '?':
            {
                table.Has(key) ? answers.emplace_back("OK") : answers.emplace_back("FAIL") ;
                cout << answers[size] << endl;
                //table.Print();
                ++size;
                break;
            }
            case '+':
            {
                table.Add(key) ? answers.emplace_back("OK") : answers.emplace_back("FAIL");
                cout << answers[size] << endl;
                //table.Print();
                ++size;
                break;
            }

            case '-':
            {
                table.Delete(key) ? answers.emplace_back("OK") : answers.emplace_back("FAIL");
                cout << answers[size] << endl;
                //table.Print();
                ++size;
                break;
            }


        }
    }


    for(int64_t i = 0 ; i < size ; ++i)
        cout << answers[i] << endl;


    return 0;
}